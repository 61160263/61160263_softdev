package OX;


import OX.Player;
import OX.Table;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {

    private Scanner kb = new Scanner(System.in);
    Player o = null;
    Player x = null;
    Table table = null;
    private int row = 0;
    private int col = 0;

    public Game() {
        this.o = new Player('O');
        this.x = new Player('X');
    }

    private int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public void newGame() {
        if (getRandomNumber(1, 100) % 2 == 0) {
            this.table = new Table(o, x);
        } else {
            this.table = new Table(x, o);
        }
    }

    public void run() {
        while (true) {
            this.runOnce();
            if (!askContinue()) {
                return;
            }
        }
    }

    public void runOnce() {
        this.showWelcome();
        this.table = new Table(o, x);
        while (true) {
            this.showTable();
            this.showTurn();
            this.inputRowCol();
            if (table.checkWin()) {
                if (table.getWinner() != null) {
                    this.showTable();
                    this.showWin();
                    this.showStat();
                } else {
                    System.out.println("Draw");
                    this.showStat();
                }
                return;
            }
            table.switchPlayer();
        }
    }

    private void showWelcome() {
        System.out.println("Welcome To OX Game");
    }

    private void showTable() {
        char[][] table = this.table.getTable();
        System.out.println("  1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < table.length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println();
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " Turn ");
    }

    private void input() {
        while (true) {
            try {
                System.out.println("Please Input Row Col : ");
                this.row = kb.nextInt();
                this.col = kb.nextInt();
                return;
            } catch (InputMismatchException iE) {
                kb.next();
                System.out.println("Please input number 1-3!!");
            }
        }
    }

    private void inputRowCol() {
        while (true) {
            this.input();
            try {
                if (table.setRowCol(row, col)) {
                    return;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Please input number 1-3!!");
            }
        }
    }

    private boolean askContinue() {
        while (true) {
            System.out.println("Continue Y/N");
            String ans = kb.next();
            if (ans.equals("N")) {
                return false;
            } else if (ans.equals("Y")) {
                return true;
            }
        }
    }

    private void showStat() {
        System.out.println(o.getName() + " (win,lose,draw): " + o.getWin() + "," + o.getLose() + "," + o.getDraw());
        System.out.println(x.getName() + " (win,lose,draw): " + x.getWin() + "," + x.getLose() + "," + x.getDraw());
    }
    
    private void showWin(){
        System.out.println(table.getWinner().getName() + " Win");
    }
}
