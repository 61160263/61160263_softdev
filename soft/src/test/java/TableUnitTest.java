/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import OX.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import OX.Table;
/**
 *
 * @author pet00
 */
public class TableUnitTest {
    public void testRow1win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(1,2);
        table.setRowCol(1,3);
        assertEquals(true,table.checkWin());
    }
    public void testRow2win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(2,1);
        table.setRowCol(2,2);
        table.setRowCol(2,3);
        assertEquals(true,table.checkWin());
    }
    public void testRow3win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(3,1);
        table.setRowCol(3,2);
        table.setRowCol(3,3);
        assertEquals(true,table.checkWin());
    }
    public void testCol1win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(2,1);
        table.setRowCol(3,1);
        assertEquals(true,table.checkWin());
    }
    public void testCol2win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,2);
        table.setRowCol(2,2);
        table.setRowCol(3,2);
        assertEquals(true,table.checkWin());
    }
    public void testCol3win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,3);
        table.setRowCol(2,3);
        table.setRowCol(3,3);
        assertEquals(true,table.checkWin());
    }
    public void testX1(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(2,2);
        table.setRowCol(3,3);
        assertEquals(true,table.checkWin());
    }
    public void testX2(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,3);
        table.setRowCol(2,2);
        table.setRowCol(3,1);
        assertEquals(true,table.checkWin());
    }
    public void testSwichPlayer(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.switchPlayer();
        assertEquals('x',table.getCurrentPlayer().getName());
    }
    public void testgetCurrentPlayer(){
        Player x = new Player('x');
        assertEquals('x',x.getName());
    }
}

